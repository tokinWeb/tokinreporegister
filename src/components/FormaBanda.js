import React from 'react'
import { PropTypes } from 'react'
import { Component } from 'react'
import PageTemplate from './PageTemplate'
import '../css/formaBanda.css';
import axios from 'axios';

class FormaBanda extends Component {
    constructor(props){
        super(props);
        this.submit = this.submit.bind(this)

        nombre:React.PropTypes.string
        genero:React.PropTypes.string
        correo:React.PropTypes.string
        tel_movil:React.PropTypes.string
        tipo:React.PropTypes.int
        password:React.PropTypes.string
        estatus:React.PropTypes.int
        imagen:React.PropTypes.string
        direccion_id:React.PropTypes.int
        fecha_registro:React.PropTypes.string
        fecha_act:React.PropTypes.string
    }

    static propTypes = {
        onNewBand:PropTypes.func.isRequired
    };


    static defaultProps = {
        onNewBand: () => {}
    }    
    submit(e){
        e.preventDefault();
        const {_nombre, _genero, _correo, _tel_movil, _tipo, _password, _estatus, _imagen,_direccion_id,_latitud,_longitud,_direccion,_fecha_registro,_fecha_act} = this.refs;
        this.props.onNewBand(_nombre.value, _genero.value, _correo.value, _tel_movil.value, _password.value, _imagen.value, _direccion.value, _longitud.value, _latitud.value);
        axios.post('http://localhost:8080/v1/address', {
        direccion: _direccion.value,
        longitud: _longitud.value,
        latitud: _latitud.value
        })  

       

        
        .then(function (response) {
        console.log(response);


         axios.get('http://localhost:8080/v1/addressi/'+_longitud.value+"/"+_latitud.value).then(res=>
        

        
        axios.post('http://localhost:8080/v1/user', {
        nombre: _nombre.value,
        genero: _genero.value,
        correo: _correo.value,
        tel_movil: _tel_movil.value,
        tipo: 3,
        password: _password.value,
        imagen: _imagen.value,
        direccion_id: res.data.id,
            })
        )

         })
        .catch(function (error) {
        console.log(error);
        });


    }

    render(){
        return (
           <div id="contact-form2">
              <div>
                <h1>Ingresa los datos de tu banda</h1> 
              </div>
              <p id="failure">Oopsie...message not sent.</p>  
              <p id="success">Your message was sent successfully. Thank you!</p>
              <form className="form" onSubmit={this.submit}>
              <div>                 
                <label for="message">
                  <span class="required">Link de imagen de la banda:  *</span> 
                    <input type="text" ref="_imagen" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>
              <div>                 
                <label for="message">
                  <span class="required">Nombre de la banda: *</span> 
                  <input type="text" ref="_nombre" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>
              <div>                 
                <label for="message">
                  <span class="required">Género: *</span> 
                  <input type="text" ref="_genero" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>

              <div>                 
                <label for="message">
                  <span class="required">Correo: *</span> 
                  <input type="text" ref="_correo" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>
              <div>                 
                <label for="message">
                  <span class="required">Teléfono móvil: *</span> 
                  <input type="text" ref="_tel_movil" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>

               <div>                 
                <label for="message">
                  <span class="required">Direccion: *</span> 
                  <input type="text" ref="_direccion" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>

               <div>                 
                <label for="message">
                  <span class="required">Longitud: *</span> 
                  <input type="text" ref="_longitud" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>
              
              <div>                 
                <label for="message">
                  <span class="required">Latitud: *</span> 
                  <input type="text" ref="_latitud" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>

              <div>                 
                <label for="message">
                  <span class="required">Contraseña: *</span> 
                  <input type="password" ref="_password" id="message" name="message" tabindex="5" required="required"></input> 
                </label>  
              </div>

              <div>                  
                <button name="submit" type="submit" id="submit" >Empieza a rockear</button> 
              </div>
              </form>
          </div> 
        )
    }

}

export default FormaBanda;