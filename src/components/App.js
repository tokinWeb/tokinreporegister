import React, { Component } from 'react';
import '../css/bootstrap.min.css';
import '../css/mainMenu.css';
import { BrowserRouter, Route } from 'react-router-dom'
import MainMenu from './MainMenu.js'
import Home from './Home'
import FormaBanda from './FormaBanda.js'

import FormaBar from './FormaBar.js'
import Registro from './Registro.js'
import { withCookies } from 'react-cookie';
import cookie from 'react-cookie';
import axios from 'axios';

class App extends Component {

   constructor( props) {
        super( props);
        this.state = {usuario:{}};
    }


/**
 componentWillMount(){
    const { cookies } = this.props;
        let code = getParameterByName("code");
        if(cookies.get('accesstoken') === undefined && code === null){
            window.location.href = "http://localhost:8081/oauth/authorize?response_type=code&client_id=demo&redirect_uri=http://localhost:3000";
        } else if(code !== null){
            axios.create({
                headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
            }).post("http://localhost:8081/oauth/token","grant_type=authorization_code&client_id=demo&redirect_uri=http://localhost:3000&code="+code)
                .then(res => {
                    console.log(res);
                    cookies.set("accesstoken",res.data.access_token);
                    localStorage.setItem('accesstoken' , res.data.access_token);
                    this.getUserData();
                }).catch(() => {
                    console.log("Error");
                window.location.href = "/"
                })
        } else if(cookies.get('accesstoken') !== undefined){
            this.getUserData();
        }
  }

  getUserData(){
        const { cookies } = this.props;
        axios.create({
            headers:{'Authorization':'Bearer '+cookies.get('accesstoken')}
        }).get("http://localhost:8081/user")
            .then(res => {
                //console.log(res.data.principal);
                this.setState({usuario:res.data.principal});
                localStorage.setItem('tipo_usuario' , res.data.principal.nombre);
            })
            .catch(() => {
                cookies.remove("accesstoken");
            })
    }**/

  render() {
    return (
        <BrowserRouter>
            <div className =" main" >
                <MainMenu/>

                <Route exact path ="/" component ={Home} />
                <Route path ="/registrate" component ={Registro} />
          
                <Route path ="/formaBar" component ={FormaBar} />

                <Route path ="/formaBanda" component ={FormaBanda} />

            </div>
        </BrowserRouter>

    );
  }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export default withCookies(App);



