import { Component } from 'react'
import { Link } from 'react-router-dom'
import '../css/bootstrap.min.css';
import React from 'react'
import PageTemplate from './PageTemplate'
import tokin from '../images/tokin.jpg'
import FormaBanda from './FormaBanda.js'
import FormaBar from './FormaBar.js'
import '../css/registro.css';

class Registro extends Component {

    render(){
            return (
                <div id="centrado">
                    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>
                    <div>
                        <img id="imgtokin" src={tokin} alt="Tokin"></img>
                    </div>
                    <div id="eltit2">¡TOKÍN!</div>
                    <div>
                        <button id="rockstar" class="button button1"> <Link to ="/formaBanda" style={{color: 'white'}}>Soy rockstar</Link></button>
                    </div>
                    <div>
                        <button id="bar" class="button button2"><Link to ="/formaBar" style={{color: 'white'}}>Necesito una banda</Link></button>
                    </div>
                </div> 
            )
    }
}

export default Registro;