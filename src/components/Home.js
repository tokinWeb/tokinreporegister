import { Component } from 'react'
import React from 'react';
import PageTemplate from './PageTemplate'
import FaSmileO from 'react-icons/lib/fa/smile-o'
import FaComments from 'react-icons/lib/fa/comments'
import FaDollar from 'react-icons/lib/fa/dollar'
import guitar from '../images/guitar.jpg'
import concert from '../images/concert.jpg'
import '../css/home.css';

class Home extends Component {

    render(){
        return (
          <section>
            <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>
              <div>
                <img id="guitar" src={guitar}></img>
                  <div id="eltit">¡TOKÍN!</div>
                  <div className="center1">
                    Encuentra bares que requieran de <br />
                    a buena música que tú les puedes ofrecer.<br />
                    Habla con ellos y acuerda el día y la hora.<br />
                    ¡Ambos pasarán un momento inolvidable!<br />
                  </div>
              </div>

              <div>
                <h2 id="text">TOKÍN</h2>
                  <p id="text2">Amamos la música</p>
                    <div id="texto">
                      <FaSmileO/><br />
                        Es fácil
                    </div> 
                    <div id="texto2">
                      <FaComments/><br />
                          Da tu opinión
                    </div> 
                    <div id="texto3">
                      <FaDollar/><br />
                      Es gratis
                    </div>                                     
              </div>
              <div>
                <img id="concert" src={concert}></img>
                  <div className="center2">¿Quisieras animar un poco más tu bar?<br />
                    Encuentra bandas de diferentes géneros<br /> que están dispuestos a hacerle pasar un<br /> buen rato
                    a todos tus invitados.
                  </div>
              </div>
          </section> 
        )
    }
}

export default Home;