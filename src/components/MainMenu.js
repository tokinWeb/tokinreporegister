import React from 'react';
import HomeIcon from 'react-icons/lib/fa/home'
import { NavLink } from 'react-router-dom'
import tokin from '../images/tokin.jpg'
import '../css/mainMenu.css';

const selectedStyle = { color: "white" };

export const MainMenu = () =>
    <nav className ="main-menu">
        <NavLink to ="/" style="font-size:30px;">  <img id="menutokin" src={tokin} alt="Tokin"></img></ NavLink >
       <a href="http://localhost:3000/">Ya estoy registrado</a>
        <NavLink to ="/registrate" activeStyle ={ selectedStyle} > Registrate </NavLink>
    </nav>;

export default MainMenu;

